# Spring-Kafka example project

This is project is a demostration on how to use the spring-kafka library
to interact with kafka using spring

This application listen for messages on a kafka topic using the class `kafka.test.handler.KafkaMessageHandler`

This application also has `kafka.test.controller.kafkaController`, a rest controller where a user can send `JSON` messages that will be then forwarder to the topic where the previous class is listening

This project demonstrate the following

- Serialization from POJO to JSON
- Deserialization from JSON to POJO
- Handling multiple message from the same topic

# Running Kafka

The project includes a docker compose to run kafka in a docker environment

To run it just navigate to the `docker-compose` folder and run the following command

    docker-compose -f docker-compose.yml up

or if you want to run it in the background

    docker-compose -f docker-compose.yml up -d

# Postman

This project also includes some rest api calls to show that with this method you can handle different type of objects and that spring-kafka can deserialize correctly every known objects that are handled in `kafka.test.handler.KafkaMessageHandler`

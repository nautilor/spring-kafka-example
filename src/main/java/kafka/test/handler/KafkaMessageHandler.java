package kafka.test.handler;

import kafka.test.model.Person;
import kafka.test.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@KafkaListener(id="kafka-topic", topics = {"kafka-topic"})
public class KafkaMessageHandler {
    Logger logger = LoggerFactory.getLogger(KafkaMessageHandler.class);

    @KafkaHandler
    public void user(User user) {
        logger.info("====== Kafka received User [ " + user.getUsername() + " ] ======");
    }

    @KafkaHandler
    public void users(List<User> users) {
        logger.info("====== Kafka received List of User [ " + users.size() + " ] ======");
    }

    @KafkaHandler
    public void person(Person person) {
        logger.info("====== Kafka received Person [ " + person.getFirstName() + " ] ======");
    }

    @KafkaHandler(isDefault = true)
    public void unknown(Object object) {
        logger.info("====== Kafka received unknown object [ " + object + " ] ======");
    }
}


package kafka.test.controller;

import kafka.test.model.Person;
import kafka.test.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    Logger logger = LoggerFactory.getLogger(KafkaController.class);

    private final String KAFKA_TOPIC = "kafka-topic";

    @Autowired
    KafkaTemplate<Object, Object> template;

    @PostMapping("/user")
    public void kafkaUser(@RequestBody User user) {
        logger.info("====== Rest received [ " + user.getUsername() + " ] ======");
        this.template.send(KAFKA_TOPIC, user);
        logger.info("====== Send to kafka done ======");
    }

    @PostMapping("/user/bulk")
    public void kafkaUsers(@RequestBody List<User> users) {
        logger.info("====== Rest received multi [ " + users.size() + " ] ======");
        this.template.send(KAFKA_TOPIC, users);
        logger.info("====== Send to kafka done ======");
    }

    @PostMapping("/person")
    public void kafkaPerson(@RequestBody Person person) {
        logger.info("====== Rest received person [ " + person.getFirstName() + " ] ======");
        this.template.send(KAFKA_TOPIC, person);
        logger.info("====== Send to kafka done ======");
    }

    @PostMapping("/object")
    public void kafkaObject(@RequestBody Object object) {
        logger.info("====== Rest received [ " + object + " ] ======");
        this.template.send(KAFKA_TOPIC, object);
        logger.info("====== Send to kafka done ======");
    }
}
